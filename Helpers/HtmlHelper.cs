﻿using System;
using System.Collections.Generic;
using HtmlAgilityPack;
using System.Linq;
using Rsdn.LFTracker.JetBrains.Annotations;

namespace Rsdn.LFTracker
{
	public static class HtmlHelper
	{
		public static IEnumerable<HtmlNode> NextNodes(this HtmlNode node)
		{
			if (node == null) throw new ArgumentNullException("node");
			var cur = node.NextSibling;
			while (cur != null)
			{
				yield return cur;
				cur = cur.NextSibling;
			}
		}

		public static IEnumerable<HtmlNode> NextNodes(this HtmlNode node, string name)
		{
			return NextNodes(node).Where(n => n.Name == name);
		}

		[CanBeNull]
		public static HtmlNode NextNode(this HtmlNode node, string name)
		{
			return node.NextNodes(name).FirstOrDefault();
		}
	}
}