﻿using System;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace Rsdn.LFTracker
{
	public partial class ShowDescriptionPage
	{
		private readonly LostFilmLoader _loader = new LostFilmLoader();

		public ShowDescriptionPage()
		{
			InitializeComponent();
		}

		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			var id = int.Parse(NavigationContext.QueryString["showPageId"]);
			_loader.LoadShowDescAsync(id, DataLoaded, LoadError);
		}

		private void LoadError(Exception obj)
		{
			throw new NotImplementedException();
		}

		private void DataLoaded(ShowDescription desc)
		{
			HeaderProgress.Visibility = Visibility.Collapsed;
			TitleText.Text = desc.Name;
			PosterImage.Source = new BitmapImage(desc.PosterUri);
			AboutText.Text = desc.About;
		}

		private void OKClick(object sender, EventArgs e)
		{
			NavigationService.GoBack();
		}
	}
}