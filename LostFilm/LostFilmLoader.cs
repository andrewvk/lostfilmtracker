﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using HtmlAgilityPack;

namespace Rsdn.LFTracker
{
	public class LostFilmLoader
	{
		private readonly WebClient _webClient = new WebClient();
		private static readonly Regex _pageUriRx = new Regex("cat=(\\d+)");
		private static readonly Uri _baseUri = new Uri("http://lostfilm.tv");

		public LostFilmLoader()
		{
			_webClient.Encoding = Win1251Encoding.Instance;
		}

		public void CancelAsync()
		{
			_webClient.CancelAsync();
		}

		private int GetPageIDFromUri(string uri)
		{
			return int.Parse(_pageUriRx.Match(uri).Groups[1].Value);
		}

		private void LoadHtmlAsync(
			Uri uri,
			Action<HtmlDocument> onHtmlLoaded,
			Action<Exception> onError)
		{
			_webClient.DownloadStringCompleted +=
				(s, args) =>
				{
					if (args.Cancelled)
						return;
					if (args.Error != null)
					{
						onError(args.Error);
						return;
					}
					try
					{
						var doc = new HtmlDocument();
						doc.Load(new StringReader(args.Result));
						onHtmlLoaded(doc);
					}
					catch (Exception ex)
					{
						onError(ex);
					}
				};
			_webClient.DownloadStringAsync(uri);
		}

		public void LoadShowsAsync(Action<ShowInfo> onShowLoaded, Action onLoadingComplete, Action<Exception> onError)
		{
			LoadHtmlAsync(
				new Uri(_baseUri, "serials.php"),
				doc =>
				{
					var infos =
					doc
						.DocumentNode
						.Descendants("div")
						.First(e => e.ChildAttributes("class").Any(a => a.Value == "bb"))
						.Elements("a")
						.Select(
							ae =>
								new ShowInfo(
									ae.ChildNodes.OfType<HtmlTextNode>().Select(t => t.InnerText).FirstOrDefault(),
									ae.Elements("span").Select(ts => ts.InnerText).FirstOrDefault(),
									GetPageIDFromUri(ae.GetAttributeValue("href", ""))))
						.ToArray();
					foreach (var info in infos)
						onShowLoaded(info);
					onLoadingComplete();
				},
				onError);
		}

		public void LoadShowDescAsync(int showPageId, Action<ShowDescription> onShowLoaded, Action<Exception> onError)
		{
			LoadHtmlAsync(
				new Uri(_baseUri, "browse.php?cat=" + showPageId),
				doc =>
				{
					var div =
						doc
							.DocumentNode
							.Descendants("div")
							.First(e => e.ChildAttributes("class").Any(a => a.Value == "mid"));
					var txtDiv = div.Element("div");
					onShowLoaded(
						new ShowDescription(
							txtDiv.Element("h1").InnerText,
							new Uri(_baseUri, txtDiv.Element("img").GetAttributeValue("src", "")),
							txtDiv
								.Elements("div")
								.First(e => e.ChildAttributes("class").Any(a => a.Value == "content"))
								.NextNode("span")
								.InnerText));
				},
				onError);
		}
	}
}