﻿namespace Rsdn.LFTracker
{
	public class ShowInfo
	{
		private readonly string _name;
		private readonly string _originalName;
		private readonly int _descPageId;

		public ShowInfo(string name, string originalName, int descPageId)
		{
			_name = name;
			_originalName = originalName;
			_descPageId = descPageId;
		}

		public string Name
		{
			get { return _name; }
		}

		public string OriginalName
		{
			get { return _originalName; }
		}

		public int DescPageId
		{
			get { return _descPageId; }
		}
	}
}