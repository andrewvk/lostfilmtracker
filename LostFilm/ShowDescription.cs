﻿using System;

namespace Rsdn.LFTracker
{
	public class ShowDescription
	{
		private readonly string _name;
		private readonly Uri _posterUri;
		private readonly string _about;

		public ShowDescription(string name, Uri posterUri, string about)
		{
			_name = name;
			_posterUri = posterUri;
			_about = about;
		}

		public string Name
		{
			get { return _name; }
		}

		public Uri PosterUri
		{
			get { return _posterUri; }
		}

		public string About
		{
			get { return _about; }
		}
	}
}