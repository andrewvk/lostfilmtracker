﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;
using Microsoft.Phone.Shell;

namespace Rsdn.LFTracker
{
	public partial class ShowSelectionPage
	{
		private readonly ObservableCollection<ShowInfo> _shows = new ObservableCollection<ShowInfo>();
		private readonly LostFilmLoader _loader = new LostFilmLoader();

		public ShowSelectionPage()
		{
			InitializeComponent();
			ShowList.ItemsSource = _shows;
		}

		protected override void OnNavigatedTo(NavigationEventArgs e)
		{
			_loader.LoadShowsAsync(info => _shows.Add(info), LoadingModeOff, OnLoadError);
		}

		private void OnLoadError(Exception ex)
		{
			HeaderProgress.Visibility = Visibility.Collapsed;
			HeaderText.Text = "Load error";
			ErrorText.Visibility = Visibility.Visible;
			ErrorText.Text = ex.Message;
		}

		private void LoadingModeOff()
		{
			HeaderProgress.Visibility = Visibility.Collapsed;
			HeaderText.Text = "Select shows";
			ApplicationBar
				.Buttons
				.OfType<ApplicationBarIconButton>()
				.First(b => b.Text == "OK")
				.IsEnabled = true;
		}

		private void OKClick(object sender, EventArgs e)
		{
			NavigationService.GoBack();
		}

		private void CancelClick(object sender, EventArgs e)
		{
			NavigationService.GoBack();
		}

		protected override void OnNavigatedFrom(NavigationEventArgs e)
		{
			_loader.CancelAsync();
		}

		private void ItemTap(object sender, GestureEventArgs e)
		{
			if (ShowList.SelectedItem == null)
				return;
			var show = (ShowInfo)ShowList.SelectedItem;
			NavigationService.Navigate(
				new Uri("/ShowDescriptionPage.xaml?showPageId=" + show.DescPageId, UriKind.Relative));
		}
	}
}