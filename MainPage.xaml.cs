﻿using System;

namespace LostFilmTracker
{
	public partial class MainPage
	{
		// Constructor
		public MainPage()
		{
			InitializeComponent();
		}

		private void AddClick(object sender, EventArgs e)
		{
			NavigationService.Navigate(new Uri("/ShowSelectionPage.xaml", UriKind.Relative));
		}
	}
}